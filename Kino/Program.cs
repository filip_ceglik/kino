﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace Kino
{
    class Program
    {
        static void Main(string[] args)
        {
            KinoContext context = new KinoContext();
            //Jeden do wielu: Kino - Sala, Sala - Miejsce, Miejsce - Bilet, Seans - Bilet
            //Jeden do zero/jeden: Kino - Adres (kino i adres powinny współdzielić klucz główny)
            Console.WriteLine("Hello World!");
        }
    }
}