using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Microsoft.EntityFrameworkCore;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace Kino
{
    public partial class KinoContext: DbContext   
    {  
        public KinoContext()
        {
        }

        public KinoContext(DbContextOptions<KinoContext> options)
            : base(options)
        {
            
        } 
        
        public virtual System.Data.Entity.DbSet<Adres> Adresy { get; set; }
        public virtual System.Data.Entity.DbSet<Bilet> Bilety { get; set; }
        public virtual System.Data.Entity.DbSet<Kino> Kina { get; set; }
        public virtual System.Data.Entity.DbSet<Miejsce> Miejsca { get; set; }
        public virtual System.Data.Entity.DbSet<Sala> Sale { get; set; }
        public virtual System.Data.Entity.DbSet<Seans> Seanse { get; set; }
  
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=localhost;Database=Kino;User Id=sa;");
            }
        }
    } 
}