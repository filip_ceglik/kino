using System;

namespace Kino
{
    public class Adres
    {
        public string miasto { get; set; }
        public string ulica { get; set; }
        public int numerDomu { get; set; }
        public string numerTel { get; set; }
        public string mail { get; set; }
    }
}