using System.Collections.Generic;

namespace Kino
{
    public class Miejsce
    {
        public int rzad { get; set; }
        public int numer { get; set; }
        
        public ICollection<Bilet> bilety { get; set; }
    }
}