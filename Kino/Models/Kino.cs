using System.Collections.Generic;

namespace Kino
{
    public class Kino
    {
        public string nazwa { get; set; }
        //public Adres adres { get; set; }
        
        public virtual ICollection<Sala> Sale { get; set; }
        public virtual Adres adres { get; set; }
    }
}