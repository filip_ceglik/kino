using System.Collections.Generic;

namespace Kino
{
    public class Sala
    {
        public string nazwa { get; set; }
        public int iloscMiejsc { get; set; }
        
        public virtual ICollection<Miejsce> miejsca { get; set; }
    }
}