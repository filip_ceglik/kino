using System;
using System.Collections.Generic;

namespace Kino
{
    public class Seans
    {
        public string nazwaFilmu { get; set; }
        public DateTime startTime { get; set; }
        
        public virtual ICollection<Bilet> bilety { get; set; }
    }
}